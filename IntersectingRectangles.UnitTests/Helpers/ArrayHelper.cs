﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IntersectingRectangles.UnitTests.Helpers
{
    public static class ArrayHelper
    {
        public static IEnumerable<IEnumerable<T>> GetAllCombinationsWithLengthEqualToOrGreaterThan<T>(this IEnumerable<T> values, int size)
        {
            //Implementation taken from https://stackoverflow.com/questions/30081908/c-sharp-linq-combinatorics-all-combinations-of-a-set-without-the-empty-set
            if (size > values.Count())
            {
                return new List<List<T>>();
            }

            return Enumerable
              .Range(1, (1 << values.Count()) - 1)
              .Select(index => values.Where((item, idx) => ((1 << idx) & index) != 0))
              .Where(combination => combination.Count() >= size).ToList();
        }
    }
}
