using IntersectingRectangles.Models;
using IntersectingRectangles.Services;
using IntersectingRectangles.UnitTests.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IntersectingRectangles.UnitTests
{
    [TestClass]
    public class IntersectionServiceTests
    {
        [TestMethod]
        public void TwoIntersectingRectanglesReturnOnlyOneSegment()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 100,
                Y = 100,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle2 = new Rectangle
            {
                Id = 2,
                X = 90,
                Y = 90,
                DeltaX = 150,
                DeltaY = 150
            };
            var intersections = new IntersectionService()
                .FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2 });
            Assert.IsTrue(intersections.Count() == 1);
        }

        [TestMethod]
        public void IntersectionWithZeroAreaIgnored()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 0,
                Y = 100,
                DeltaX = 100,
                DeltaY = 100
            };

            var rectangle2 = new Rectangle
            {
                Id = 2,
                X = 0,
                Y = 0,
                DeltaX = 100,
                DeltaY = 100
            };

            var rectangle3 = new Rectangle
            {
                Id = 3,
                X = 100,
                Y = 0,
                DeltaX = 100,
                DeltaY = 100
            };

            var rectangle4 = new Rectangle
            {
                Id = 4,
                X = 0,
                Y = 0,
                DeltaX = 100,
                DeltaY = 100
            };

            var intersectionService = new IntersectionService();
            var zeroAreaTopBottomIntersection = intersectionService
                .FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2 }).FirstOrDefault();
            Assert.IsNull(zeroAreaTopBottomIntersection);
            var zeroAreaLeftRightIntersection = intersectionService
                .FindIntersectionsForRectangles(new List<Rectangle> { rectangle3, rectangle4 }).FirstOrDefault();
            Assert.IsNull(zeroAreaLeftRightIntersection);
        }

        [TestMethod]
        public void DisjointRectanglesHaveNoIntersections()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 60,
                Y = 0,
                DeltaX = 200,
                DeltaY = 100
            };

            var rectangle2 = new Rectangle
            {
                Id = 2,
                X = 0,
                Y = 120,
                DeltaX = 180,
                DeltaY = 80
            };

            var rectangle3 = new Rectangle
            {
                Id = 3,
                X = 200,
                Y = 110,
                DeltaX = 250,
                DeltaY = 80
            };

            var rectangle4 = new Rectangle
            {
                Id = 4,
                X = 80,
                Y = 210,
                DeltaX = 350,
                DeltaY = 190
            };

            var intersectionService = new IntersectionService();
            var findIntersectionsResult = intersectionService.FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2, rectangle3, rectangle4 });
            Assert.IsFalse(findIntersectionsResult.Any());
        }

        [TestMethod]
        public void TwoIdenticalRectanglesReturnOneIdenticalIntersection()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 0,
                Y = 0,
                DeltaX = 100,
                DeltaY = 100
            };

            var rectangle2 = new Rectangle
            {
                Id = 2,
                X = 0,
                Y = 0,
                DeltaX = 100,
                DeltaY = 100
            };

            var intersectionService = new IntersectionService();
            var intersectionOrNull = intersectionService.FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2 }).FirstOrDefault();
            Assert.IsNotNull(intersectionOrNull);
            Assert.IsTrue(rectangle1.Equals(intersectionOrNull.Rectangle));
            Assert.IsTrue(rectangle2.Equals(intersectionOrNull.Rectangle));
        }

        [TestMethod]
        public void NegativeCoordinatesReturnTheSameSizedIntersectionAsPositive()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 100,
                Y = 100,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle2 = new Rectangle
            {
                Id = 2,
                X = 90,
                Y = 90,
                DeltaX = 150,
                DeltaY = 150
            };
            var rectangle3 = new Rectangle
            {
                Id = 4,
                X = -90,
                Y = -90,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle4 = new Rectangle
            {
                Id = 5,
                X = -100,
                Y = -100,
                DeltaX = 150,
                DeltaY = 150
            };

            var intersectionService = new IntersectionService();
            var positiveIntersection = intersectionService
                .FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2 }).First();
            var negativeIntersection = intersectionService
                .FindIntersectionsForRectangles(new List<Rectangle> { rectangle3, rectangle4 }).First();

            Assert.IsTrue(positiveIntersection.Rectangle.DeltaX == negativeIntersection.Rectangle.DeltaX &&
                        positiveIntersection.Rectangle.DeltaY == negativeIntersection.Rectangle.DeltaY);
        }

        [TestMethod]
        public void DuplicateIntersectionsAreNotDiscarded()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 100,
                Y = 100,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle2 = new Rectangle
            {
                Id = 2,
                X = 90,
                Y = 90,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle3 = new Rectangle
            {
                Id = 3,
                X = 100,
                Y = 100,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle4 = new Rectangle
            {
                Id = 4,
                X = 90,
                Y = 90,
                DeltaX = 150,
                DeltaY = 150
            };
            var intersections = new IntersectionService()
                .FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2, rectangle3, rectangle4 });
            var firstTwoRectangleIntersections = intersections.Where(i => i.OrderedIntersectingRectangleIds.SequenceEqual(new int[] { 1, 2 }));
            var lastTwoRectangleIntersections = intersections.Where(i => i.OrderedIntersectingRectangleIds.SequenceEqual(new int[] { 3, 4 }));
            Assert.IsTrue(firstTwoRectangleIntersections.Count() == 1);
            Assert.IsTrue(lastTwoRectangleIntersections.Count() == 1);
            Assert.IsTrue(firstTwoRectangleIntersections.First().Rectangle.Equals(lastTwoRectangleIntersections.First().Rectangle));
        }

        [TestMethod]
        public void RectangleListWithDuplicateIdsThrowsException()
        {
            var rectangle1 = new Rectangle
            {
                Id = 1,
                X = 100,
                Y = 100,
                DeltaX = 150,
                DeltaY = 150
            };

            var rectangle2 = new Rectangle
            {
                Id = 1,
                X = 90,
                Y = 90,
                DeltaX = 150,
                DeltaY = 150
            };

            Assert.ThrowsException<InvalidOperationException>(() => new IntersectionService().FindIntersectionsForRectangles(new List<Rectangle> { rectangle1, rectangle2 }));
        }

        [TestMethod]
        public void MultipleDuplicateLayersHaveAllPossibleIntersectionsCounted()
        {
            var ids = Enumerable.Range(1, new Random().Next(2, 6)).ToArray();
            var combinations = ids.GetAllCombinationsWithLengthEqualToOrGreaterThan(2);
            var rectangles = ids.Select(id => new Rectangle
            {
                Id = id,
                X = 90,
                Y = 90,
                DeltaX = 150,
                DeltaY = 150
            }).ToList();
            var intersections = new IntersectionService().FindIntersectionsForRectangles(rectangles);
            var intersectionsIds = intersections.Select(i => i.OrderedIntersectingRectangleIds);
            Assert.IsTrue(intersections.Count() == combinations.Count());
            foreach (var id in intersectionsIds)
            {
                Assert.IsTrue(combinations.Any(c => c.SequenceEqual(id)));
            }
        }
    }
}
