﻿using IntersectingRectangles.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IntersectingRectangles.UnitTests
{
    [TestClass]
    public class RectangleServiceTests
    {
        private string GenerateJsonStringFromRectangles(List<dynamic> rectangles)
        {
            var rectangleJsonStrings = "";
            foreach (var rectange in rectangles)
            {
                rectangleJsonStrings += "{      " +
                                                $"\"x\": {rectange.X}," +
                                                $"\"y\": {rectange.Y}," +
                                                $"\"delta_x\": {rectange.DeltaX}," +
                                                $"\"delta_y\": {rectange.DeltaY}" +
                                        " },";
            }
            return "{  \"rects\": [" + rectangleJsonStrings + "]  }";
        }

        private List<dynamic> CreateRandomListOfRectangles(int size = 10)
        {
            var random = new Random();
            var rectangles = new List<dynamic>();
            for (var i = 0; i < size; i++)
            {
                rectangles.Add(new
                {
                    X = random.Next(),
                    Y = random.Next(),
                    DeltaX = random.Next(0, 1000),
                    DeltaY = random.Next(0, 1000)
                });
            }
            return rectangles;
        }

        [TestMethod]
        public void ExtraRectanglesGetTruncated()
        {
            var smallMaximum = 3;
            var largeMaximum = 10;
            var rectangleCount = smallMaximum + 1;
            var rectangleService = new RectangleService(smallMaximum);
            var jsonString = GenerateJsonStringFromRectangles(CreateRandomListOfRectangles(rectangleCount));
            Assert.IsTrue(rectangleService.ParseRectanglesFromJsonString(jsonString).Count() == smallMaximum);
            rectangleService = new RectangleService(largeMaximum);
            Assert.IsTrue(rectangleService.ParseRectanglesFromJsonString(jsonString).Count() == rectangleCount);
        }

        [TestMethod]
        public void ParsedRectanglesHaveUniqueId()
        {
            var rectangleService = new RectangleService();
            var jsonString = GenerateJsonStringFromRectangles(CreateRandomListOfRectangles());
            var rectangles = rectangleService.ParseRectanglesFromJsonString(jsonString);
            var ids = rectangles.Select(r => r.Id);
            var uniqueIds = ids.OrderBy(a => a).Distinct();
            Assert.IsTrue(ids.Count() == ids.Count());
        }

        [TestMethod]
        public void NegativeLengthThrowsException()
        {
            var rectangleService = new RectangleService();
            var random = new Random();
            var exceptionMessage = "Side lengths must be positive integers";
            var rectangleConstructionObject = new
            {
                X = random.Next(),
                Y = random.Next(),
                DeltaX = -10,
                DeltaY = random.Next(0, 1000)
            };
            var rectangleConstructionList = new List<dynamic>() { rectangleConstructionObject };
            var jsonString = GenerateJsonStringFromRectangles(rectangleConstructionList);
            var exception = Assert.ThrowsException<ArgumentException>(() => rectangleService.ParseRectanglesFromJsonString(jsonString));
            Assert.IsTrue(exception.Message == exceptionMessage);
            rectangleConstructionObject = new
            {
                X = random.Next(),
                Y = random.Next(),
                DeltaX = random.Next(0, 1000),
                DeltaY = -10
            };
            rectangleConstructionList = new List<dynamic>() { rectangleConstructionObject };
            jsonString = GenerateJsonStringFromRectangles(rectangleConstructionList);
            exception = Assert.ThrowsException<ArgumentException>(() => rectangleService.ParseRectanglesFromJsonString(jsonString));
            Assert.IsTrue(exception.Message == exceptionMessage);
        }


        [TestMethod]
        public void IncorrectlyFormedJsonThrowsException()
        {
            var rectangleService = new RectangleService();
            var jsonString = GenerateJsonStringFromRectangles(CreateRandomListOfRectangles());
            jsonString = jsonString.Substring(1);
            var exception = Assert.ThrowsException<ArgumentException>(() => rectangleService.ParseRectanglesFromJsonString(jsonString));
            Assert.IsTrue(exception.Message == "JSON input is improperly formatted and can not be parsed");
        }
    }
}
