# Intersecting Rectangles #

### Overview ###
This application reads in a JSON file containing the definitions of up to 10 rectangles. It then calculates the set of all intersections between these rectangles and displays this information back to the user. Each rectangle passed in is defined by an x and a y coordinate as well as a length and a width. All coordinates supplied must be integer values and all lengths and widths must be positive integers.

### How to build ###
This project was developed using the .NET 5.0 SDK. To build and run it you will need to install the SDK for your particular environment.
The SDK can be found at this [link](https://dotnet.microsoft.com/download) and will come bundled with all the required tools to build the solution. 
Once you have installed the SDK, you should be able to build the solution to your desired output directory using the CLI (command line interface).
Open up a command line session, navigate to the source location that contains the .sln file.
Run the following command to build the solution and output the application to your specified folder: 
```bash
dotnet build .\IntersectingRectangles.sln --output "C:/[DesiredOutputDirectory]"
```

### Running the application ###
To run the application once the project has been built, you must first navigate to the output directory supplied to the build command and then run the following command, replacing the file path with a path to your input JSON file:
```bash
.\IntersectingRectangles.exe "C:/[PathToRectanglesJsonFile]"
```

### JSON Schema ###
The JSON file that needs to be supplied has to follow the schema that is included in the app. 
For convenience the schema is shown below:
```
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "rects": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "x": {
            "type": "integer"
          },
          "y": {
            "type": "integer"
          },
          "delta_x": {
            "type": "integer"
          },
          "delta_y": {
            "type": "integer"
          }
        },
        "required": [
          "x",
          "y",
          "delta_x",
          "delta_y"
        ]
      }
    }
  },
  "required": [
    "rects"
  ]
}
```

An example of JSON input using this schema is shown below:
```
{
  "rects": [
    {
      "x": 100,
      "y": 100,
      "delta_x": 250,
      "delta_y": 80
    },
    {
      "x": 120,
      "y": 200,
      "delta_x": 250,
      "delta_y": 150
    },
    {
      "x": 140,
      "y": 160,
      "delta_x": 250,
      "delta_y": 100
    },
    {
      "x": 160,
      "y": 140,
      "delta_x": 350,
      "delta_y": 190
    },
    {
      "x": 80,
      "y": 180,
      "delta_x": 180,
      "delta_y": 80
    },
    {
      "x": 140,
      "y": 120,
      "delta_x": 200,
      "delta_y": 100
    }
  ]
}

```

### Contact ###

This repository is a personal codebase maintained by Matt Grant. To contact please email nzmattgrant@gmail.com.