﻿namespace IntersectingRectangles.Helpers
{
    public static class StringHelpers
    {
        public static string ReplaceLastOccurrence(this string source, string toReplace, string replaceWith)
        {
            var replacementIndex = source.LastIndexOf(toReplace);

            if (replacementIndex == -1) 
                return source;

            var result = source.Remove(replacementIndex, toReplace.Length).Insert(replacementIndex, replaceWith);
            return result;
        }

    }
}
