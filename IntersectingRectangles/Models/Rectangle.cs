﻿using Newtonsoft.Json;
using System;
using System.Diagnostics.CodeAnalysis;

namespace IntersectingRectangles.Models
{
    public class Rectangle : IEquatable<Rectangle>
    {
        [JsonProperty("x")]
        public int  X { get; set; }

        [JsonProperty("y")]
        public int Y { get; set; }

        [JsonProperty("delta_x")]
        public int DeltaX { get; set; }

        [JsonProperty("delta_y")]
        public int DeltaY { get; set; }

        public int Id { get; set; }

        public int Left => X;

        public int Right => X + DeltaX;

        public int Top => Y + DeltaY;

        public int Bottom => Y;

        public bool IsDisjointWith(Rectangle otherRectangle)
        {
            return Right <= otherRectangle.Left || otherRectangle.Right <= Left ||
                Top <= otherRectangle.Bottom || otherRectangle.Top <= Bottom;
        }

        public bool Equals([AllowNull] Rectangle otherRectangle)
        {
            if (otherRectangle == null)
            {
                return false;
            }
            return X == otherRectangle.X &&
                Y == otherRectangle.Y &&
                DeltaX == otherRectangle.DeltaX &&
                DeltaY == otherRectangle.DeltaY;
        }

        //Needed for equality comparison to work correctly, equal objects are guaranteed to have the same hashcode.
        //relying on the built-in object hashcode for equality comparison can result in two objects that pass the 
        //equality comparison being considered unequal as they have different hashcodes
        public override int GetHashCode()
        {
            return X ^ Y ^ DeltaX ^ DeltaY;
        }

        public override string ToString()
        {
            return $"Rectangle at ({X},{Y}), delta_x={DeltaX}, delta_y={DeltaY}";
        }
    }
}
