﻿using IntersectingRectangles.Helpers;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace IntersectingRectangles.Models
{
    public class Intersection : IEquatable<Intersection>
    {
        public Rectangle Rectangle { get; set; }

        public int[] IntersectingRectangleIds { get; set; }

        public int[] OrderedIntersectingRectangleIds => IntersectingRectangleIds.OrderBy(id => id).ToArray();

        private string IdListToString()
        {
            return string.Join(", ", IntersectingRectangleIds).ReplaceLastOccurrence(", ", " and ");
        }

        public bool Equals([AllowNull] Intersection otherIntersection)
        {
            if (otherIntersection == null)
            {
                return false;
            }
            return Rectangle.Equals(otherIntersection.Rectangle) &&
                OrderedIntersectingRectangleIds.SequenceEqual(otherIntersection.OrderedIntersectingRectangleIds);
        }

        public override int GetHashCode()
        {
            return Rectangle.GetHashCode();
        }

        public override string ToString()
        {
            return $"Between rectangles {IdListToString()} at ({Rectangle.X},{Rectangle.Y}), delta_x={Rectangle.DeltaX}, delta_y={Rectangle.DeltaY}";
        }
    }
}
