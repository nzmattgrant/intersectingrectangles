﻿using IntersectingRectangles.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace IntersectingRectangles.Services
{
    public class RectangleService
    {
        private readonly int maximumRectangles;
        private readonly JSchema schema;

        public RectangleService(int maximumRectangles = 10)
        {
            this.maximumRectangles = maximumRectangles;
            using (StreamReader reader = File.OpenText("schema.json"))
            {
                var schemaString = reader.ReadToEnd();
                schema = JSchema.Parse(schemaString);
            }
        }

        public List<Rectangle> ParseRectanglesFromJsonString(string jsonString)
        {
            try
            {
                var parsedJson = JObject.Parse(jsonString);
                var isValidJson = parsedJson.IsValid(schema);
                if (isValidJson == false)
                {
                    throw new ArgumentException($"JSON input does not follow the required rectangle schema: {schema}");
                }

                var rectangles = JsonConvert.DeserializeObject<Dictionary<string, Rectangle[]>>(jsonString)["rects"];
                for (var i = 0; i < rectangles.Length; i++)
                {
                    if (rectangles[i].DeltaX == 0 || rectangles[i].DeltaY == 0)
                    {
                        throw new ArgumentException("Provided rectangle has a size of length 0");
                    }
                    if (rectangles[i].DeltaX < 0 || rectangles[i].DeltaY < 0)
                    {
                        throw new ArgumentException("Side lengths must be positive integers");
                    }
                    rectangles[i].Id = i + 1;
                }
                var rectanglesList = rectangles.Length > maximumRectangles ? 
                    rectangles.Take(maximumRectangles).ToList() : rectangles.ToList();

                return rectanglesList;
            }
            catch (Exception ex) when (ex is JsonSerializationException || ex is JsonReaderException)
            {
                throw new ArgumentException("JSON input is improperly formatted and can not be parsed");
            }
        }
    }
}
