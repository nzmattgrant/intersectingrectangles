﻿using IntersectingRectangles.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IntersectingRectangles.Services
{
    public class IntersectionService
    {
        public List<Intersection> FindIntersectionsForRectangles(List<Rectangle> rectangles)
        {
            var ids = rectangles.Select(r => r.Id);
            if(ids.Distinct().Count() != ids.Count())
            {
                throw new InvalidOperationException("All rectangle objects must have a unique Id");
            }

            var intersections = new List<Intersection>();

            rectangles.ForEach((rectangle1) =>
            {
                rectangles.ForEach((rectangle2) =>
                {
                    if (rectangle1.Id == rectangle2.Id) 
                        return;

                    var intersectionRectangle = GetIntersectionRectangleOrNull(rectangle1, rectangle2);

                    if (intersectionRectangle == null) 
                        return;

                    intersections.Add(new Intersection
                    {
                        Rectangle = intersectionRectangle,
                        IntersectingRectangleIds = new int[] { rectangle1.Id, rectangle2.Id }
                    });
                });
            });

            var distinctIntersections = intersections.Distinct().ToList();
            var recursiveIntersections = FindIntersectionsRecursive(rectangles, distinctIntersections);
            return recursiveIntersections.Distinct().ToList();
        }

        private List<Intersection> FindIntersectionsRecursive(List<Rectangle> rectangles, List<Intersection> intersections)
        {
            if(intersections.Any() == false)
            {
                return new List<Intersection>();
            }

            var newIntersections = new List<Intersection>();
            rectangles.ForEach((rectangle) =>
            {
                intersections.ForEach((intersection) =>
                {
                    if (intersection.IntersectingRectangleIds.Contains(rectangle.Id)) 
                        return;

                    var intersectionRectangle = GetIntersectionRectangleOrNull(rectangle, intersection.Rectangle);

                    if (intersectionRectangle == null) 
                        return;

                    newIntersections.Add(new Intersection
                    {
                        Rectangle = intersectionRectangle,
                        IntersectingRectangleIds = new int[] { rectangle.Id }.Concat(intersection.IntersectingRectangleIds).ToArray()
                    });
                });
            });

            var recursiveIntersections = FindIntersectionsRecursive(rectangles, newIntersections);
            return intersections.Concat(newIntersections.Concat(recursiveIntersections)).Distinct().ToList();
        }

        private Rectangle GetIntersectionRectangleOrNull(Rectangle rectangle1, Rectangle rectangle2)
        {
            if (rectangle1.IsDisjointWith(rectangle2))
            {
                return null;
            }

            var intersectionTop = Math.Min(rectangle1.Top, rectangle2.Top);
            var intersectionLeft = Math.Max(rectangle1.Left, rectangle2.Left);

            var intersectionBottom = Math.Max(rectangle1.Bottom, rectangle2.Bottom);
            var intersectionRight = Math.Min(rectangle1.Right, rectangle2.Right);

            var intersectionRectangle = new Rectangle
            {
                X = intersectionLeft,
                Y = intersectionBottom,
                DeltaX = intersectionRight - intersectionLeft,
                DeltaY = intersectionTop - intersectionBottom
            };
            return intersectionRectangle;
        }
    }
}
