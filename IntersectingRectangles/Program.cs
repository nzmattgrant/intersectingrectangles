﻿using IntersectingRectangles.Services;
using System;
using System.IO;

namespace IntersectingRectangles
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("No JSON file path provided to application");
                return;
            }

            var filePath = args[0];
            if (File.Exists(filePath) == false)
            {
                Console.WriteLine($"The JSON file at {filePath} does not exist");
                return;
            }

            using (StreamReader reader = File.OpenText(filePath))
            {
                try
                {
                    var jsonString = reader.ReadToEnd();
                    var rectangles = new RectangleService().ParseRectanglesFromJsonString(jsonString);
                    Console.WriteLine("Input:");
                    rectangles.ForEach(rectangle => Console.WriteLine($"    {rectangle.Id}: {rectangle}"));
                    var intersections = new IntersectionService().FindIntersectionsForRectangles(rectangles);
                    Console.WriteLine("Intersections:");
                    intersections.ForEach(intersection => Console.WriteLine($"    {intersection}"));
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
